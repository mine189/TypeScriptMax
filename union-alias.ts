type Comb = number | string;
type ConvDesc = 'as-String' | 'as-Number';

function combine (
    input1:Comb,
    input2:Comb,
    resCon:ConvDesc,
    ){
    let result;
    if (typeof input1==='number' && typeof input2==='number'||resCon==='as-Number'){
        result=+input1 + +input2;
    } else {
        result=input1.toString()+input2.toString();
    }
    return result;
}

const combNumb=combine(30,20,'as-Number');
console.log(combNumb);

const combWord=combine('Zdrao','Sokole','as-String');
console.log(combWord);
