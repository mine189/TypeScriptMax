function add (n1:number, n2:number,showRes:boolean, phrase:string){
    // if(typeof n1!=='number' || typeof n2!=='number'){
    //     throw new Error('ERROR: incorrect input!!! :(')
    // }
    if (showRes) {
        console.log(phrase, n1+n2)
    } else {
        return n1+n2;
    }
}

const number1=4;
const number2=2.8;
const printRes=true
const resPhrase='Obori:'

add(number1, number2,printRes,resPhrase);
